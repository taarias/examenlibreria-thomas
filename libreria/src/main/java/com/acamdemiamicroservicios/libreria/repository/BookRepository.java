package com.acamdemiamicroservicios.libreria.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.acamdemiamicroservicios.libreria.entity.Book;

public interface BookRepository extends JpaRepository<Book, Long> {

}
