package com.acamdemiamicroservicios.libreria.proxy;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.acamdemiamicroservicios.libreria.entity.Book;

@FeignClient(name = "libreria", url = "http://localhost:8080")
//@Bean
public interface LibreriaProxy {
	
	//Metodo get all 
	@GetMapping("/book/{bookId}")
	public Book retrieveBook(
			
			@PathVariable Long bookId);
	
	
	
	

}
