package com.acamdemiamicroservicios.libreria.controller;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.acamdemiamicroservicios.libreria.entity.Book;
import com.acamdemiamicroservicios.libreria.proxy.LibreriaProxy;
import com.acamdemiamicroservicios.libreria.service.BookService;

@RestController
public class BooksController {
	
	@Autowired
	BookService bookService;
	
	//@Autowired
	private LibreriaProxy proxy;
	
	
	//Metodos CRUD
	
	//Metodo get all 
	@GetMapping("/book")
	private List<Book> getAll(){
		return bookService.getAll();
	}
	
	
	//Metodo getByID para libro en especifico  
	@GetMapping("/book/{bookId}")  
	private Book getBook(@PathVariable("bookId") Long bookId)   {  
	return bookService.getBookById(bookId);  
	}  
	
	//Metodo Post
	@PostMapping("/books")  
	private Long saveBook(@RequestBody Book book)   {  
	bookService.save(book);  
	return book.getId();  
	}  
	
	
	//Metodo Put para actualizar un libro   
	@PutMapping("/books")  
	private Book update(@RequestBody Book book)   {  
	bookService.save(book);  
	return book;  
	}
	
	//intente agregar un proxy despues de la sesion con manu, pero no me dio tiempo de terminarlo
	//Get BookByID con proxy
	/*
	 * 	@GetMapping("/book/{bookId}")
	public Book getEspecialBookFeign(
			@PathVariable Long bookId
			) {

		Book book = proxy.retrieveBook(bookId);
		
		return new Book(book.getId(),
				book.getAuthor(),
				book.getName(),
				book.getPrice());
		
	}
	 * */
	

	
}
