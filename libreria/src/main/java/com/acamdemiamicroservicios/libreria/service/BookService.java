package com.acamdemiamicroservicios.libreria.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acamdemiamicroservicios.libreria.entity.Book;
import com.acamdemiamicroservicios.libreria.repository.BookRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BookService {
	
	@Autowired
	BookRepository bookRepository;
	
	public List<Book> getAll(){
		
		List<Book> book = new ArrayList<Book>();
		bookRepository.findAll().forEach(book1 -> book.add(book1));
		return book;
	}
	
	//Metodo que regresa un libro en especifico
	public Book getBookById(Long id) {
		return bookRepository.findById(id).get();
	}
	
	//Metodo que guarda un libro
	public void save(Book book)   {
		log.info("guardar libro en Book service");
		bookRepository.save(book);  
	}  
	
	//Metodo que actualiza un libro
	public void update(Book book, Long bookId){
		log.info("actualizar libro en Book service");
		bookRepository.save(book);  
	}  

}
